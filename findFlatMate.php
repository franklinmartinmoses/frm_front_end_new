<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<title>Fresher rooms</title>
		<link href="assets/lib/bootstrap.css" rel="stylesheet">
		<link href="assets/styles/styles.css" rel="stylesheet">
		<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
	</head>
	<body>
		<?php 
			include "assets/views/header.php";
			include "assets/views/findmateForm.php";			
		?>
		
		
		<div class="container wrapper">
			<div class="row wrapperContainer">
				<div class="col-md-12">
					<div class="featureBox">
						<span> < BACK</span>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-10 borderRight">
					<?php include "assets/views/flatmateContainer.php";?>
				</div>
				<div class="col-md-2 ">
					<?php 
						include "assets/views/newStudentContainer.php";
					?>
				</div>
			</div>
		</div>
		<?php include "assets/views/footer.php"?>
	</body>
	<script src="assets/lib/jquery.js"></script>
	<script src="assets/lib/jquery.bxslider.js"></script>
	<script src="assets/scripts/frm.js"></script>
</html>	