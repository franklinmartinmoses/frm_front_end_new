<div class="row borderBottom">
						<div class="col-md-8 borderRight">
							<h4><span class="frmColor">3 BEDROOM FLAT, WAPPING (E1) -</span><span>1500 PCM (BILLS INC.)</span></h4>
							<div class="col-md-5">
								<div class="propImg">
									<img src="assets/images/pollock_halls_070209_aw01.jpg"/>
									<div class="imgCount" rel="4">
										<span>4</span><span class="glyphicon glyphicon-picture"></span>
									</div>
								</div>
							</div>
							<div class="col-md-7">
								<p>2 doubles (ensuite), 1 single</p>
								<p>2 bathrooms</p>
								<p>Large kitchen / living room</p>
								<p>Roof terrace</p>
								<p>Small garden (shared)</p>
								<p>1/4 mile (12 mins walk) from Goldsmiths College</p>
								<div class="featureBox">
									<span>Bookmark</span>
								</div>
								<div class="featureBox">
									<span>Tag</span>
								</div>
								<div class="featureBox">
									<span>View Tags</span>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<h4><span class="frmColor">CONTACT ADVERTISER</span></h4>
							<p><span class="glyphicon glyphicon-earphone frmColor"></span> 07550253488</p>
							<p><span class="glyphicon glyphicon-envelope frmColor"></span> adasds@gmail.com</p>
							<img src="assets/images/maps.jpg"/>
						</div>
					</div>
					<div class="row borderBottom">	
						<div class="col-md-8 borderRight">
							<h4><span class="frmColor">3 BEDROOM FLAT, WAPPING (E1) -</span><span>1500 PCM (BILLS INC.)</span></h4>
							<div class="col-md-5">
								<div class="propImg">
									<img src="assets/images/pollock_halls_070209_aw01.jpg"/>
									<div class="imgCount">
										<span>4 </span><span class="glyphicon glyphicon-picture"></span>
									</div>
								</div>
							</div>
							<div class="col-md-7">
								<p>2 doubles (ensuite), 1 single</p>
								<p>2 bathrooms</p>
								<p>Large kitchen / living room</p>
								<p>Roof terrace</p>
								<p>Small garden (shared)</p>
								<p>1/4 mile (12 mins walk) from Goldsmiths College</p>
								<div class="featureBox">
									<span>Bookmark</span>
								</div>
								<div class="featureBox">
									<span>Tag</span>
								</div>
								<div class="featureBox">
									<span>View Tags</span>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<h4><span class="frmColor">CONTACT ADVERTISER</span></h4>
							<p><span class="glyphicon glyphicon-earphone frmColor"></span> 07550253488</p>
							<p><span class="glyphicon glyphicon-envelope frmColor"></span> adasds@gmail.com</p>
							<img src="assets/images/maps.jpg"/>
						</div>
					</div>