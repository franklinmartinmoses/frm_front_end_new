<div class="row">
						<div class="col-md-12  borderBottom">
							<h4><span class="frmColor">TOM PAYNE, 20</span></h4>
						</div>
					</div>
					<div class="row wrapperContainer borderBottom">
						<div class="col-md-3 profImg">
							<img src="assets/images/facebook-blank-photo.jpg"/>
						</div>
						<div class="col-md-5 borderRight">
							<h4><span class="frmColor">ABOUT</span></h4>
							<table class="detailsTable">
								<tr>
									<td class="frmColor">Studying:</td>
									<td>Msc </td>
								</tr>
								<tr>
									<td class="frmColor">University:</td>
									<td>UCL </td>
								</tr>
								<tr>
									<td class="frmColor">Interests:</td>
									<td>Cycling, reading, night out</td>
								</tr>
							</table>
							<h4><span class="frmColor">LOOKING FOR</span></h4>
							<table class="detailsTable">
								<tr>
									<td class="frmColor">No. beds:</td>
									<td>Msc </td>
								</tr>
								<tr>
									<td class="frmColor">In/Near:</td>
									<td>UCL </td>
								</tr>
								<tr>
									<td class="frmColor">Rent:</td>
									<td>Cycling, reading, night out</td>
								</tr>
								<tr>
									<td class="frmColor">Comment:</td>
									<td>If you're interested text me</td>
								</tr>
							</table>
						</div>
						<div class="col-md-4">
							<h4><span class="frmColor">TOM'S TAGS(14)</span></h4>
							<div class="tagDetails">
								<img src="assets/images/pollock_halls_070209_aw01.jpg" class="tagImg"/>
								<p class="frmColor">2 BED FLAT, WAPPING </p>
								<p>&pound;160 pw</p>
							</div>
							<div class="tagDetails">
								<img src="assets/images/pollock_halls_070209_aw01.jpg" class="tagImg"/>
								<p class="frmColor">2 BED FLAT, WAPPING </p>
								<p>&pound;160 pw</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12  borderBottom">
							<h4><span class="frmColor">SIMILAR STUDENT TO TOM</span></h4>
						</div>
						<div class="col-md-12 ">
							<div class="row wrapperContainer">
								<div class="similarMates col-md-2">
									<img src="assets/images/facebook-blank-photo.jpg"/>
									<p class="frmColor">Alice, 19</p>
									<p class="frmColor">LSE</p>
								</div>
								<div class="similarMates col-md-2">
									<img src="assets/images/facebook-blank-photo.jpg"/>
									<p class="frmColor">Alice, 19</p>
									<p class="frmColor">LSE</p>
								</div>
								<div class="similarMates col-md-2">
									<img src="assets/images/facebook-blank-photo.jpg"/>
									<p class="frmColor">Alice, 19</p>
									<p class="frmColor">LSE</p>
								</div>
								<div class="similarMates col-md-2">
									<img src="assets/images/facebook-blank-photo.jpg"/>
									<p class="frmColor">Alice, 19</p>
									<p class="frmColor">LSE</p>
								</div>
								<div class="similarMates col-md-2">
									<img src="assets/images/facebook-blank-photo.jpg"/>
									<p class="frmColor">Alice, 19</p>
									<p class="frmColor">LSE</p>
								</div>
								<div class="similarMates col-md-2">
									<img src="assets/images/facebook-blank-photo.jpg"/>
									<p class="frmColor">Alice, 19</p>
									<p class="frmColor">LSE</p>
								</div>
							</div>
						</div>
					</div>