<div id="findroomForm">
				<div class="container">
					<div class="row searchForm">
						<div class="col-md-6">
								<table class="frmTable">
									<tr>
										<td><label>UNIVERSITY/LOCATION</label></td>
										<td><input type="text" class="form-control"></td>
									</tr>
									<tr>
										<td><label>SEARCH RADIUS</label></td>
										<td>
											<select class="form-control">
											  <option>1</option>
											  <option>2</option>
											  <option>3</option>
											  <option>4</option>
											  <option>5</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><label>NO. OF BEDS</label></td>
										<td><input type="text" class="form-control smallBox" > TO <input type="text" class="form-control smallBox"></td>
									</tr>
								</table>
							</form>
						</div>
						<div class="col-md-6">
								<table class="frmTable">
									<tr>
										<td><label>RENT (&pound;)</label></td>
										<td><input type="text" class="form-control smallBox"> TO <input type="text" class="form-control smallBox"></td>
									</tr>
									<tr>
										<td><label>BILLS INCLUDED</label></td>
										<td><input type="checkbox" ></td>
									</tr>
									<tr>
										<td><button type="submit" class="btn btn-primary">SEARCH</button></td>
									</tr>
								</table>
							</form>
						</div>
					</div>
				</div>
		</div>